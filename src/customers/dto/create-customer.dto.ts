import { IsNotEmpty, IsPositive } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  @IsPositive()
  point: number;
}
